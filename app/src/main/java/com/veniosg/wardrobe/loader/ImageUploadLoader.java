package com.veniosg.wardrobe.loader;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.entity.ImageUploadResponse;
import com.veniosg.wardrobe.net.service.PictureService.TypedImageOutput;
import com.veniosg.wardrobe.net.service.ServiceFactory;
import com.veniosg.wardrobe.util.Utils;

/**
 * Loader that uploads an image to the wardrobe server.
 */
public class ImageUploadLoader extends AsyncTaskLoader<Void> {
	private Garment mGarment;

	public ImageUploadLoader(Context context, Garment garment) {
		super(context);

		this.mGarment = garment;
	}

	@Override
	protected void onStartLoading() {
//		Log.d(ImageUploadLoader.class.getName(), "Started loading");
		forceLoad();
	}

	@Override
	protected void onStopLoading() {
//		Log.d(ImageUploadLoader.class.getName(), "Finished loading");
		cancelLoad();
	}

	@Override
	public Void loadInBackground() {
		// Upload image
		ImageUploadResponse resp = ServiceFactory.getPictureService()
				.uploadImage(
						new TypedImageOutput(getContext(),
								Constants.MIME_IMAGE,
								Uri.parse(mGarment.getPicture().getPath())));

		// Update entity
		mGarment.getPicture().setPath(resp.getPath());

		if(mGarment.getPicture().getId() != null) {
			// Update server-side Picture representation, as it would otherwise not be cascaded since it contains an id.
			ServiceFactory.getPictureService().update(mGarment.getPicture());
		}
		return null;
	}
}