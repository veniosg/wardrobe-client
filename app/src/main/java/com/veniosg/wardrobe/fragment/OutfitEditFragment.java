package com.veniosg.wardrobe.fragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.adapter.OutfitEditViewAdapter;
import com.veniosg.wardrobe.entity.Outfit;
import com.veniosg.wardrobe.entity.OutfitItem;
import com.veniosg.wardrobe.net.service.ServiceFactory;
import com.veniosg.wardrobe.util.ZOrderAwareList;
import com.veniosg.wardrobe.view.OutfitDisplayView;

public class OutfitEditFragment extends EntityEditFragment<Outfit> {
	private EditText mTitleView;
	private SeekBar mXView;
	private SeekBar mYView;
	private SeekBar mRotateView;
	private SeekBar mScaleView;
	private OutfitDisplayView mItemsView;
	
	// This is responsible for keeping the items and their state. 
	// Use OutfitEditViewAdapter#getItems() to get the outfit state at any moment.
	private OutfitEditViewAdapter mAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// A bit different because of the possibility of non-inserted, non-empty outfits due to generation.
		if (handlesNewInstance) {
			// If outfit was null we handle it by creating a new outfit.
			mEntity = new Outfit();
		} else {
			// Handle generated outfits like new ones, but keep the passed entity
			if(mEntity.getId() == null) {
				handlesNewInstance = true;
			}
		}
		
		// Stop keyboard from popping up. Not defined in 
		// manifest/activity as it's this fragment's views causing it
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_outfit, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Keep the views
		mTitleView = (EditText) view.findViewById(R.id.title);
		mXView = (SeekBar) view.findViewById(R.id.x);
		mYView = (SeekBar) view.findViewById(R.id.y);
		mRotateView = (SeekBar) view.findViewById(R.id.rotate);
		mScaleView = (SeekBar) view.findViewById(R.id.scale);
		mItemsView = (OutfitDisplayView) view.findViewById(R.id.items);
		
		entityToViews();
		setupListeners();
		
		indicateSuccess();
	}
	
	@Override
	void create() {
		viewsToEntity();
		ServiceFactory.getOutfitService().create(mEntity, new Callback<Outfit>() {
			public void success(Outfit t, Response response) {
				mEntity = t;

				OutfitGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	void update() {
		viewsToEntity();
		ServiceFactory.getOutfitService().update(mEntity, new Callback<Response>() {
			public void success(Response t, Response response) {
				OutfitGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});		
	}
	
	@Override
	protected void done() {
		getActivity().setResult(Activity.RESULT_OK);
		
		super.done();
	}
	
	@Override
	void entityToViews() {
		mTitleView.setText(mEntity.getTitle());
		
		// First, fill the items view
		mAdapter = new OutfitEditViewAdapter(
						new ZOrderAwareList<OutfitItem>(
								mEntity.getItems()));
		mItemsView.setAdapter(mAdapter);
		
		// Then init the seekbars
		refreshSelectedItemControls();
	}

	/**
	 * Call to refresh state of the item controls. 
	 * Good places to call would be when the selection changes 
	 * and after the items view has been initialized 
	 * and therefore the initial state of the controls can be retrieved.
	 */
	private void refreshSelectedItemControls() {
		if(mAdapter.getCount() == 0) {
			// If there are no items, return.
			return;
		}
		
		OutfitItem selectedItem = mAdapter.getItem(0);	// AKA top item
		mXView.setProgress(Float.valueOf(selectedItem.getxCoord() * 100).intValue());
		mYView.setProgress(Float.valueOf(selectedItem.getyCoord() * 100).intValue());
		mRotateView.setProgress(Float.valueOf(selectedItem.getRotation() * 100).intValue());
		mScaleView.setProgress(Float.valueOf(selectedItem.getScale() * 100).intValue());
	}

	@Override
	void viewsToEntity() {
		mEntity.setTitle(mTitleView.getText().toString());
		mEntity.setItems(mAdapter.getItems());
	}
	
	private void setupListeners() {
		mXView.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
			@Override
			public void onProgressChangedFromUser(SeekBar seekBar, int progress) {
				mItemsView.setSelectedViewX(progress/100F);
			}
		});
		mYView.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
			@Override
			public void onProgressChangedFromUser(SeekBar seekBar, int progress) {
				mItemsView.setSelectedViewY(progress/100F);
			}
		});
		mRotateView.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
			@Override
			public void onProgressChangedFromUser(SeekBar seekBar, int progress) {
				mItemsView.setSelectedViewRotation(progress/100F);
			}
		});
		mScaleView.setOnSeekBarChangeListener(new SimpleOnSeekBarChangeListener() {
			@Override
			public void onProgressChangedFromUser(SeekBar seekBar, int progress) {
				mItemsView.setSelectedViewScale(progress/100F);
			}
		});
		
		mItemsView.setOnChildSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// The adapter has refreshed order by now, just renew the seekbars' values
				refreshSelectedItemControls();
			}

			public void onNothingSelected(AdapterView<?> arg0) {}
		});
	}
	
	/**
	 * Simplifies the interface of the {@link SeekBar} changes to the minimum of our needs.
	 */
	private abstract class SimpleOnSeekBarChangeListener implements OnSeekBarChangeListener {

		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			// Just ignore programmatic changes as it's us refreshing state indication
			if(fromUser) {
				onProgressChangedFromUser(seekBar, progress);
			}
		}

		public void onStartTrackingTouch(SeekBar seekBar) {}

		public void onStopTrackingTouch(SeekBar seekBar) {}
		
		public abstract void onProgressChangedFromUser(SeekBar seekBar, int progress);
	}
}
