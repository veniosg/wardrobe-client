package com.veniosg.wardrobe.fragment;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.activity.GarmentDebugActivity;
import com.veniosg.wardrobe.adapter.TypeSpinnerAdapter;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.entity.Garment.Weather;
import com.veniosg.wardrobe.entity.Picture;
import com.veniosg.wardrobe.entity.Type;
import com.veniosg.wardrobe.loader.ImageUploadLoader;
import com.veniosg.wardrobe.net.service.ServiceFactory;
import com.veniosg.wardrobe.util.NotificationFactory;
import com.veniosg.wardrobe.util.Utils;
import com.veniosg.wardrobe.view.DynamicSizeEditTextGroup;

public class GarmentEditFragment extends EntityEditFragment<Garment> implements LoaderManager.LoaderCallbacks<Void> {
	private static final int PICTURE_REQUEST_CODE = 1;
	private static final int LOADER_ID_IMAGE_UPLOAD = 1;
	
	private ImageButton mPictureView;
	private EditText mNameView;
	private Spinner mTypeView;
	private DynamicSizeEditTextGroup mTagsView;
	private Spinner mWeatherView;
	private CheckBox mNewlyBoughtView;
	private Button mDebugButton;
	
	private boolean mHasNewImage = false;
	
	private List<Type> mAllTypes;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(handlesNewInstance) {
			// If garment was null we handle it by creating a new garment.
			mEntity = new Garment();
		}
		
		// Stop keyboard from popping up. Not defined in 
		// manifest/activity as it's this fragment's views causing it
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_garment, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Keep the views
		mPictureView = (ImageButton) view.findViewById(R.id.picture);
		mNameView = (EditText) view.findViewById(R.id.name);
		mTypeView = (Spinner) view.findViewById(R.id.types);
		mTagsView = (DynamicSizeEditTextGroup) view.findViewById(R.id.tags);
		mWeatherView = (Spinner) view.findViewById(R.id.weather);
		mNewlyBoughtView = (CheckBox) view.findViewById(R.id.newlyBought);
		mDebugButton = (Button) view.findViewById(R.id.debug);
		
		if(!handlesNewInstance) {
			// If item's not new this attribute must not be user-editable.
			mNewlyBoughtView.setVisibility(View.GONE);
		}
		
		mPictureView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(Utils.isIntentAvailable(getActivity(), MediaStore.ACTION_IMAGE_CAPTURE)) {
					pickPicture();
				} else {
					Toast.makeText(getActivity(), R.string.cant_take_picture, Toast.LENGTH_LONG).show();
				}
			}
		});
		mDebugButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				viewsToEntity();
				
				Intent intent = new Intent(getActivity(), GarmentDebugActivity.class);
				intent.putExtra(Constants.EXTRA_ENTITY, mEntity);
				startActivity(intent);
			}
		});
		
		initLoadAllTypes();
	}
	
	private void initLoadAllTypes() {
		GarmentEditFragment.this.indicateLoading();
		ServiceFactory.getTypeService().list(new Callback<List<Type>>() { 
			public void success(List<Type> t, Response response) {
				if(getActivity() == null) {
					// User has navigated back, ignore everything and leave
					return;
				}
				
				if(t == null || t.size() == 0) {
					indicateFailure(getString(R.string.no_types));
				} else {
					mAllTypes = t;
					entityToViews();
					indicateSuccess();
				}
			}
			
			public void failure(RetrofitError error) {
				indicateFailure(error.getMessage());
			}
		});	
	}
	
	@Override
	/**
	 * Overridden so that we first handle the picture uploading if needed. 
	 * Upon finish of the Loader, we'll call the actual super method that 
	 * in turn calls down to create() or update() appropriately.
	 */
	protected void done() {
		if(mHasNewImage) {
			// Start loader
			getActivity().getSupportLoaderManager()
			.initLoader(LOADER_ID_IMAGE_UPLOAD, null, this);
			NotificationFactory.showUploadUpdate(getActivity());
		} else {
			super.done();
		}
	}
	
	@Override
	void create() {
		viewsToEntity();
		ServiceFactory.getGarmentService().create(mEntity, new Callback<Garment>() {
			public void success(Garment t, Response response) {
				mEntity = t;

				GarmentGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});	
	}

	@Override
	void update() {
		viewsToEntity();

		ServiceFactory.getGarmentService().update(mEntity, new Callback<Response>() {
			public void success(Response t, Response response) {
				GarmentGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	void entityToViews() {
		if(!TextUtils.isEmpty(mEntity.getPicture().getPath())) {
			Picasso
			.with(getActivity())
            // If the image is new, the path is actually a local uri and we must use it as is.
			.load(mHasNewImage ? mEntity.getPicture().getPath()
                    : Utils.getFullUrl(mEntity.getPicture().getPath()))
			.into(mPictureView);
		}
		mNameView.setText(mEntity.getName());
		mTagsView.setContentData(mEntity.getTags());
		mWeatherView.setSelection(mEntity.getWeather().ordinal());
		mNewlyBoughtView.setChecked(mEntity.getRecentlyPurchased());
		mTypeView.setAdapter(new TypeSpinnerAdapter(getActivity(),
                        android.R.layout.simple_spinner_item, mAllTypes));
        ((ArrayAdapter<?>) mTypeView.getAdapter()).setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mTypeView.setSelection(mAllTypes.indexOf(mEntity.getType()));
	}

	@Override
	void viewsToEntity() {
		// Picture setting is handled in #onActivityResult(...);
		mEntity.setName(mNameView.getText().toString());
		mEntity.setType((Type) mTypeView.getSelectedItem());
		mEntity.setWeather(Weather.values()[mWeatherView.getSelectedItemPosition()]);
		mEntity.setRecentlyPurchased(mNewlyBoughtView.isChecked());
		mEntity.setTags(mTagsView.getContentData());
	}

	/**
	 * Request picking a picture. Launches the gallery or the SAF on >4.4.
	 */
	@SuppressLint("InlinedApi")
	private void pickPicture() {
		Intent intent = new Intent();
		intent.setType(Constants.MIME_IMAGE);
		intent.addCategory(Intent.CATEGORY_OPENABLE);
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
		   	intent.setAction(Intent.ACTION_GET_CONTENT);
		} else {
			intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
		}
		startActivityForResult(intent, PICTURE_REQUEST_CODE);
	}
	
	/**
	 * Create a new image and put it in mEntity. 
	 * Tries to keep the id of the previous Picture instance.
	 * @param pictureUri The URI of the image to use.
	 */
	private void assignNewPictureToEntity(Uri pictureUri) {
		mHasNewImage = true;
		
		Picture picture = mEntity.getPicture();
		if(picture == null) {
			picture = new Picture();
		}
		try {
			ParcelFileDescriptor parcelFileDescriptor = 
					getActivity().getContentResolver().openFileDescriptor(pictureUri, "r");
		
			FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
			Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
			picture.setColor(Utils.getColorFromBitmap(image, 0.25F, 0.1F));
			picture.setPath(pictureUri.toString());
			
			parcelFileDescriptor.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		mEntity.setPicture(picture);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == PICTURE_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				if(data != null && data.getData() != null) {
					// Keep old view state
					viewsToEntity();
					
					assignNewPictureToEntity(data.getData());
					
					// Now update the views.
					entityToViews();
				}
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	public Loader<Void> onCreateLoader(int id, Bundle args) {
		if(id == LOADER_ID_IMAGE_UPLOAD) {
			return new ImageUploadLoader(getActivity(), mEntity);
		} else {
			throw new IllegalArgumentException("GarmentEditFragment: Unrecognised Loader ID");
		}
	}

	public void onLoadFinished(Loader<Void> loader, Void data) {
		switch (loader.getId()) {
		case LOADER_ID_IMAGE_UPLOAD:
			if(mHasNewImage) {
				NotificationFactory.showUploadFinished(getActivity());
			}
			
			mHasNewImage = false;
			
			// Image uploaded and garment state updated
			// Create or update the garment accordingly
			super.done();
			break;
		default:
			break;
		}
	}

	public void onLoaderReset(Loader<Void> loader) {
		// We don't care about loader not being available.
	}
}
