package com.veniosg.wardrobe.fragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Type;
import com.veniosg.wardrobe.net.service.ServiceFactory;

public class TypeEditFragment extends EntityEditFragment<Type> {
	private EditText mTitleView;
	private NumberPicker mAllowedView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(handlesNewInstance) {
			// If type was null we handle it by creating a new type.
			mEntity = new Type();
		}
		
		// Stop keyboard from popping up. Not defined in 
		// manifest/activity as it's this fragment's views causing it
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_type, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Keep the views
		mTitleView = (EditText) view.findViewById(R.id.title);
		mAllowedView = (NumberPicker) view.findViewById(R.id.allowed_in_outfit);
		
		mAllowedView.setMaxValue(5);
		mAllowedView.setMinValue(1);
		mAllowedView.setValue(1);
		
		entityToViews();
		
		indicateSuccess();
	}
	
	@Override
	void create() {
		viewsToEntity();
		ServiceFactory.getTypeService().create(mEntity, new Callback<Type>() {
			public void success(Type t, Response response) {
				mEntity = t;

				TypeGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	void update() {
		viewsToEntity();
		ServiceFactory.getTypeService().update(mEntity, new Callback<Response>() {
			public void success(Response t, Response response) {
				TypeGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});		
	}

	@Override
	void entityToViews() {
		mTitleView.setText(mEntity.getTitle());
		mAllowedView.setValue(mEntity.getAllowedInstancesInOutfit() == null 
				? 1
				: mEntity.getAllowedInstancesInOutfit());
	}

	@Override
	void viewsToEntity() {
		mEntity.setTitle(mTitleView.getText().toString());
		mEntity.setAllowedInstancesInOutfit(mAllowedView.getValue());
	}
}
