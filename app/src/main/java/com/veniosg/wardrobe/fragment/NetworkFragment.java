package com.veniosg.wardrobe.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.veniosg.wardrobe.R;

/**
 * Provides a handy way to change the view depending on the network/request state.
 * Subclasses MUST add their views to as the last child of the result of this class's {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}. <br/>
 * 
 * @author George Venios
 *
 */
public class NetworkFragment extends Fragment {
	private ViewFlipper mFlipper;
	private TextView mErrorTextView;
	
	public enum RequestStatus {
		LOADING,
		ERROR,
		NO_NETWORK,
		OK
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_network, null);

		mFlipper = (ViewFlipper) view.findViewById(R.id.flipper);
		mErrorTextView = (TextView) view.findViewById(R.id.errorTextView);
		
		indicateLoading();
		
		return view;
	}
	
	/**
	 * Show content pane
	 */
	public void indicateSuccess() {
		showByStatus(RequestStatus.OK);
	}
	
	public void indicateLoading() {
		showByStatus(RequestStatus.LOADING);
	}
	
	public void indicateFailure() {
		indicateFailure(null);
	}
	
	public void indicateNoNetwork() {
		showByStatus(RequestStatus.NO_NETWORK);
	}
	
	/**
	 * Same as {@link #indicateFailure()} but using a custom message.
	 * @param errorMessage A message to be displayed to the user.
	 */
	public void indicateFailure(CharSequence errorMessage) {
		if(mErrorTextView != null) {
			if(TextUtils.isEmpty(errorMessage)) {
				// Set default error text
				mErrorTextView.setText(
						mErrorTextView.getContext()
						.getString(R.string.error_loading));
			} else {
				// Set custom error text
				mErrorTextView.setText(errorMessage);
			}
		}
		
		showByStatus(RequestStatus.ERROR);
	}
	
	/**
	 * Handles showing the correct child. Won't try to flip when already displaying requested child.
	 * @param status The current status.
	 */
	private void showByStatus(RequestStatus status) {
		showByIndex(status.ordinal());
	}
	
	private void showByIndex(int childToShow) {
		if(mFlipper.getDisplayedChild() != childToShow) {
			mFlipper.setDisplayedChild(childToShow);
		}
	}
	
	/**
	 * Use this to show custom status state views 
	 * @param childToShow This must be larger than the ordinal of the last RequestStatus value.
	 * @throws IllegalArgumentException When childToShow < {@link RequestStatus#values()}.length. 
	 */
	public void showCustom(int childToShow) {
		if(childToShow < RequestStatus.values().length) {
			throw new IllegalArgumentException(childToShow + " must be larger or equal to " + RequestStatus.values().length);
		}
		
		showByIndex(childToShow);
	}

}
