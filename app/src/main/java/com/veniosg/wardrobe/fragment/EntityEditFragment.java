package com.veniosg.wardrobe.fragment;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.R;

public abstract class EntityEditFragment<E> extends NetworkFragment {
	/**
	 * Whether the fragment handles an instance 
	 * to be created or updates an existing one. <br/>
	 * Default is false.
	 */
	protected boolean handlesNewInstance = false;
	
	/**
	 * The instance this fragment is editing.
	 */
	protected E mEntity;
	
	abstract void create();
	abstract void update();
	abstract void entityToViews();
	abstract void viewsToEntity();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Inflate a "Done/Discard" custom action bar view.
        LayoutInflater inflater = (LayoutInflater) getActivity().getActionBar().getThemedContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customActionBarView = inflater.inflate(
                R.layout.actionbar_custom_view_done_discard, null);
        customActionBarView.findViewById(R.id.actionbar_done).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        done();
                    }
                });
        customActionBarView.findViewById(R.id.actionbar_discard).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        discard();
                    }
                });

        // Show the custom action bar view and hide the normal Home icon and title.
        final ActionBar actionBar = getActivity().getActionBar();
        actionBar.setDisplayOptions(
                ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView, new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        
		// Get entity to edit.
		mEntity = getArguments().getParcelable(Constants.ARG_ENTITY);
		// Set handlesNewInstance flag
		handlesNewInstance = (mEntity == null);
	}
	
	/**
	 * Call to save or update the entity to the server storage.
	 */
	protected void done() {
		if(handlesNewInstance) {
			create();
		} else {
			update();
		}
	}
	
	protected void discard() {
		getActivity().finish();
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// KitKat displays Up even on custom ActionBars if requested, 
		// so we disable it here.
		getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
}
