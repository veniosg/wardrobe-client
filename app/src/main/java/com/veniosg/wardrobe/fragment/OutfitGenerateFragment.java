package com.veniosg.wardrobe.fragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.activity.OutfitEditActivity;
import com.veniosg.wardrobe.entity.GenerationOptions;
import com.veniosg.wardrobe.entity.Outfit;
import com.veniosg.wardrobe.entity.Garment.Weather;
import com.veniosg.wardrobe.net.service.ServiceFactory;

/**
 */
public class OutfitGenerateFragment extends NetworkFragment {
	private static final int REQUEST_CODE_NEW_OUTFIT = 1;
	
	private Button mGenerateButton;
	private NumberPicker mMaxItemsView;
	private CheckBox mControlMaxView;
	private Spinner mWeatherView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_generate_outfit, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Keep the views
		mGenerateButton = (Button) view.findViewById(R.id.generate);
		mMaxItemsView = (NumberPicker) view.findViewById(R.id.max_selector);
		mControlMaxView = (CheckBox) view.findViewById(R.id.max_controller);
		mWeatherView = (Spinner) view.findViewById(R.id.weather);
		
		mMaxItemsView.setMaxValue(8);
		mMaxItemsView.setMinValue(4);
		mMaxItemsView.setValue(5);
		mWeatherView.setSelection(Weather.ANY.ordinal());
		
		setupListeners();
		indicateSuccess();
	}
	
	private void setupListeners() {
		mGenerateButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				generate();
			}
		});
		
		mControlMaxView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mMaxItemsView.setVisibility(isChecked ? View.GONE : View.VISIBLE);
			}
		});
	}
	
	/**
	 * Call to make the request to the server to generate and return the outfit with the passed parameters.
	 */
	protected void generate() {
		indicateLoading();
		
		ServiceFactory.getOutfitService().generated(getGenerationOptions(), new Callback<Outfit>() {
			public void success(Outfit t, Response response) {
				// Inform list to refresh
				OutfitGridFragment.reloadData(getActivity());
				
				// View/edit resulting outfit
				getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				Intent intent = new Intent(getActivity(), OutfitEditActivity.class);
				intent.putExtra(Constants.EXTRA_ENTITY, t);
				startActivityForResult(intent, REQUEST_CODE_NEW_OUTFIT);
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_generate, Toast.LENGTH_SHORT).show();
				
				indicateSuccess();
			}
		});
	}
	
	private GenerationOptions getGenerationOptions() {
		GenerationOptions opts = new GenerationOptions();
		opts.setMaxItems(mControlMaxView.isChecked() ? mMaxItemsView.getValue() : -1);
		opts.setWeather(Weather.values()[mWeatherView.getSelectedItemPosition()]);
		return opts;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_NEW_OUTFIT) {
			if (resultCode == Activity.RESULT_OK) {
				// Outfit was accepted by the user, close this activity.
				getActivity().finish();
			} else {
				indicateSuccess();
			}
		}
	}
}
