package com.veniosg.wardrobe.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.util.Utils;
import com.veniosg.wardrobe.util.color.ColorMatchingUtils;
import com.veniosg.wardrobe.util.color.ColorWheel;

public class GarmentDebugFragment extends NetworkFragment {
	/**
	 * The instance this fragment is editing.
	 */
	protected Garment mEntity;
	
	private ImageView mColorView;
	private TextView mRecentlyPurchasedView;
	private TextView mColorLogView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Get entity to edit.
		mEntity = getArguments().getParcelable(Constants.ARG_ENTITY);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_debug_garment,
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mColorView = (ImageView) view.findViewById(R.id.color);
		mRecentlyPurchasedView = (TextView) view.findViewById(R.id.recentlyPurchased);
		mColorLogView = (TextView) view.findViewById(R.id.colorLog);
		
		StringBuilder log = new StringBuilder();
		log.append("isGrayscale: ").append(ColorMatchingUtils.isGrayscale(mEntity.getPicture().getColor())).append('\n');
		log.append("Background is the band color of the picture color");
		
		if(!TextUtils.isEmpty(mEntity.getPicture().getPath())) {
			Picasso.with(getActivity())
				.load(Utils.getFullUrl(mEntity.getPicture().getPath()))
				.into(mColorView);
		}
		mColorView.setBackground(new ColorDrawable(mEntity.getPicture().getColor()));
		mRecentlyPurchasedView.setText(mEntity.getRecentlyPurchased().toString());
		mColorLogView.setText(log.toString());
		mColorLogView.setBackground(
				new ColorDrawable(
						ColorWheel.getBandOfColor(
								mEntity.getPicture().getColor(), 
								ColorMatchingUtils.STANDARD_NUM_BANDS)));
		
		indicateSuccess();
	}
}