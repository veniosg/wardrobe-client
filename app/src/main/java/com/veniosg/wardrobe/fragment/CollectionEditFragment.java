package com.veniosg.wardrobe.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.adapter.CheckableGarmentGridAdapter;
import com.veniosg.wardrobe.entity.Collection;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.net.service.ServiceFactory;

public class CollectionEditFragment extends EntityEditFragment<Collection> {
	private EditText mTitleView;
	private GridView mMemberView;
	
	private CheckableGarmentGridAdapter mAdapter;
	private List<Garment> mAllGarments;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(handlesNewInstance) {
			// If collection was null we handle it by creating a new collection.
			mEntity = new Collection();
		}
		
		// Stop keyboard from popping up. Not defined in 
		// manifest/activity as it's this fragment's views causing it
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_edit_collection, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		// Keep the views
		mTitleView = (EditText) view.findViewById(R.id.title);
		mMemberView = (GridView) view.findViewById(R.id.garments);

		initLoadAllGarments();
	}
	
	private void initLoadAllGarments() {
		CollectionEditFragment.this.indicateLoading();	
		ServiceFactory.getGarmentService().list(new Callback<List<Garment>>() { 
			public void success(List<Garment> t, Response response) {
				mAllGarments = t;
				entityToViews();
				indicateSuccess();
			} 
			
			public void failure(RetrofitError error) {
				indicateFailure(error.getMessage());
			}
		});	
	}

	@Override
	void create() {
		viewsToEntity();
		ServiceFactory.getCollectionService().create(mEntity, new Callback<Collection>() {
			public void success(Collection t, Response response) {
				mEntity = t;

				CollectionGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	void update() {
		viewsToEntity();
		ServiceFactory.getCollectionService().update(mEntity, new Callback<Response>() {
			public void success(Response t, Response response) {
				CollectionGridFragment.reloadData(getActivity());
				getActivity().finish();
			}
			
			public void failure(RetrofitError error) {
				error.printStackTrace();
				Toast.makeText(getActivity(), R.string.could_not_save, Toast.LENGTH_SHORT).show();
			}
		});		
	}

	@Override
	void entityToViews() {
		mTitleView.setText(mEntity.getTitle());
		
		// If we have the list data
		if(mAllGarments != null) {
			mAdapter = new CheckableGarmentGridAdapter(getActivity(), mAllGarments);
			mMemberView.setAdapter(mAdapter);
			checkSelectedItems(mMemberView, mAllGarments, mEntity.getGarments());
		} else {
			initLoadAllGarments();
		}
	}

	@Override
	void viewsToEntity() {
		mEntity.setTitle(mTitleView.getText().toString());
		mEntity.setGarments(getSelectedGarmentsOfList(mAllGarments, mMemberView.getCheckedItemIds()));
	}

	private static List<Garment> getSelectedGarmentsOfList(List<Garment> allGarments, long[] checkedItemIds) {
		ArrayList<Garment> list = new ArrayList<Garment>();
		
		Arrays.sort(checkedItemIds);
		
		// O(allgarments.size * log(checkedItemIds.length)). Can be improved.
		int foundAt = -1;
		Garment thisItem;
		for(int i = 0; i < allGarments.size(); i++) {
			thisItem = allGarments.get(i);
			foundAt = Arrays.binarySearch(checkedItemIds, thisItem.getId());
			if (foundAt >= 0) {
				list.add(thisItem);
			}
		}
		
		return list;
	}

	private void checkSelectedItems(GridView grid, List<Garment> all, List<Garment> toSelect) {
		if(toSelect == null) {
			return;
		}
		
		// Painfully slow.
		// O(n^2)
		for(int i = 0; i < all.size(); i++) {
			if(toSelect.contains(all.get(i))) {
				grid.setItemChecked(i, true);
			}
		}
	}
}
