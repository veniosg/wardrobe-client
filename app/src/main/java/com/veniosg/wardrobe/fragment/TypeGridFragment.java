package com.veniosg.wardrobe.fragment;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.activity.TypeEditActivity;
import com.veniosg.wardrobe.activity.TypeNewActivity;
import com.veniosg.wardrobe.adapter.TypeGridAdapter;
import com.veniosg.wardrobe.entity.Type;
import com.veniosg.wardrobe.net.service.ServiceFactory;
import com.veniosg.wardrobe.util.Utils;

public class TypeGridFragment extends NetworkFragment {
	private GridView mGrid;
	private TypeGridAdapter mAdapter;
	private BroadcastReceiver mReloadDataReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			indicateLoading();
			loadTypes();
		}
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		
		try {
			LocalBroadcastManager.getInstance(getActivity())
			.registerReceiver(mReloadDataReceiver, 
					new IntentFilter(Constants.ACTION_RELOAD_DATA, Constants.MIME_TYPE));
		} catch (MalformedMimeTypeException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onDestroy() {
		LocalBroadcastManager.getInstance(getActivity())
		.unregisterReceiver(mReloadDataReceiver);
		
		super.onDestroy();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		inflater.inflate(R.menu.list_types, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_type:
			addType();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_grid_type, 
				(ViewGroup) super.onCreateView(inflater, container, savedInstanceState));
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		setupViewListeners(view);
		setupViewContent(view);
		
		loadTypes();
	}
	
	private void addType() {
		getActivity().startActivity(new Intent(getActivity(), TypeNewActivity.class));
	}
	
	private void loadTypes() {
		if(Utils.hasNetwork(getActivity())) {
			ServiceFactory.getTypeService().list(new Callback<List<Type>>() {
				
				public void success(List<Type> t, Response response) {
					mAdapter = new TypeGridAdapter(getActivity(), t);
					mGrid.setAdapter(mAdapter);
					indicateSuccess();
				}
				
				public void failure(RetrofitError error) {
					error.printStackTrace();
					indicateFailure();
				}
			});
		} else {
			indicateNoNetwork();
		}
	}
	
	private void setupViewListeners(View container) {
		OnClickListener reloadListener = new OnClickListener() {
			public void onClick(View v) {
				indicateLoading();
				loadTypes();
			}
		};
		
		// Empty indicator
		container.findViewById(android.R.id.empty).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				addType();
			}
		});
		// Reload/retry buttons
		container.findViewById(R.id.reloadBtn).setOnClickListener(reloadListener);
		container.findViewById(R.id.retryBtn).setOnClickListener(reloadListener);
	}
	
	private void setupViewContent(View container) {
		mGrid = (GridView) container.findViewById(android.R.id.list);
		mGrid.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(getActivity(), TypeEditActivity.class);
				intent.putExtra(Constants.EXTRA_ENTITY, (Type) mAdapter.getItem(position));
				
				getActivity().startActivity(intent);
			}
		});
		mGrid.setEmptyView(container.findViewById(android.R.id.empty));
	}

	public static void reloadData(Context c) {
		Intent intent = new Intent(Constants.ACTION_RELOAD_DATA);
		intent.setTypeAndNormalize(Constants.MIME_TYPE);
		LocalBroadcastManager.getInstance(c).sendBroadcast(intent);
	}
}