package com.veniosg.wardrobe.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.veniosg.wardrobe.entity.OutfitItem;
import com.veniosg.wardrobe.util.Utils;
import com.veniosg.wardrobe.util.ZOrderAwareList;

public class OutfitEditViewAdapter extends BaseAdapter {
	private ZOrderAwareList<OutfitItem> items = new ZOrderAwareList<OutfitItem>(); // Avoid NPEs

	public OutfitEditViewAdapter(ZOrderAwareList<OutfitItem> items) {
		this.items = items;
	}
	
	public void setItems(ZOrderAwareList<OutfitItem> items) {
		this.items = items;
		
		notifyDataSetChanged();
	}
	
	@SuppressWarnings("unchecked")	// LOL Java, really?
	public ZOrderAwareList<OutfitItem> getItems() {
		return (ZOrderAwareList<OutfitItem>) items.clone();
	}

	public void bringToFront(int position) {
		if(position != 0) { 	// Not already in front
			items.bringToFront(position);
		}
		notifyDataSetChanged();
	}

	public int getCount() {
		return items.size();
	}

	public OutfitItem getItem(int position) {
		return items.get(position);
	}

	public long getItemId(int position) {
		Long id = items.get(position).getId();
		
		// Handle "new outfit" generation where the contained OutfitItems are not persisted.
		if(id == null) {
			return items.get(position).getGarment().getId();
		} else {
			return id;
		}
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		OutfitItem item = getItem(position);
		
		if (convertView == null) {
			convertView = new ImageView(parent.getContext());
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);

			convertView.setLayoutParams(params);
		}

		Picasso
		.with(parent.getContext())
		.load(Utils.getFullUrl(item.getGarment()
				.getPicture().getPath()))
		.error(android.R.drawable.ic_dialog_alert)
		.into((ImageView) convertView);

		int parentWidth = convertView.getWidth();
		int parentHeight = convertView.getHeight();
		
		// Pivot point being at the center of the object is the default, no need to do anything
		convertView.setScaleX(item.getScale());
		convertView.setScaleY(item.getScale());
		convertView.setRotation(item.getRotation());
		convertView.setX(item.getxCoord()*parentHeight);
		convertView.setY(item.getyCoord()*parentWidth);
		
		return convertView;
	}
}
