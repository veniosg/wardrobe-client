package com.veniosg.wardrobe.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.util.Utils;

public class GarmentGridAdapter extends BaseAdapter {
	private List<Garment> mItems = new ArrayList<Garment>();
	private LayoutInflater mInflater;
	protected int mItemLayoutResource = R.layout.item_grid_garment;

	public GarmentGridAdapter(Context c, List<Garment> t) {
		if(t != null) {
			mItems = t;
		}
		
		mInflater = LayoutInflater.from(c);
	}

	public int getCount() {
		return mItems.size();
	}

	public Object getItem(int position) {
		return mItems.get(position);
	}

	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		Garment item = (Garment) getItem(position);
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mItemLayoutResource, null);
			
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(android.R.id.text1);
			holder.pic = (ImageView) convertView.findViewById(android.R.id.icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.name.setText(item.getName());
		Picasso.with(convertView.getContext())
		.load(Utils.getFullUrl(item.getPicture().getPath()))
		.error(android.R.drawable.ic_dialog_alert)
		.into(holder.pic);
		return convertView;
	}
	
	private class ViewHolder {
		TextView name;
		ImageView pic;
	}
}
