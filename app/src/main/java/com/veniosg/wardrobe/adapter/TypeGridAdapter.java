package com.veniosg.wardrobe.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Type;

public class TypeGridAdapter extends BaseAdapter {
	private List<Type> mItems = new ArrayList<Type>();
	private LayoutInflater mInflater;

	public TypeGridAdapter(Context c, List<Type> t) {
		if(t != null) {
			mItems = t;
		}
		
		mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return mItems.size();
	}

	public Object getItem(int position) {
		return mItems.get(position);
	}

	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		Type item = (Type) getItem(position);
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(R.layout.item_grid_type, null);
			
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(android.R.id.text1);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.title.setText(item.getTitle());
		return convertView;
	}
	
	private class ViewHolder {
		TextView title;
	}
}
