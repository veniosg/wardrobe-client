package com.veniosg.wardrobe.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Collection;
import com.veniosg.wardrobe.util.Utils;

public class CollectionGridAdapter extends BaseAdapter {
	private List<Collection> mItems = new ArrayList<Collection>();
	private LayoutInflater mInflater;

	public CollectionGridAdapter(Context c, List<Collection> t) {
		if(t != null) {
			mItems = t;
		}
		
		mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return mItems.size();
	}

	public Object getItem(int position) {
		return mItems.get(position);
	}

	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		Collection item = (Collection) getItem(position);
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(R.layout.item_grid_collection, null);
			
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(android.R.id.text1);
			holder.pic = (ImageView) convertView.findViewById(android.R.id.icon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.name.setText(item.getTitle());
		// Only show first garment's picture
		if(!item.getGarments().isEmpty()) {
			Picasso.with(convertView.getContext())
			.load(Utils.getFullUrl(item.getGarments().get(0).getPicture().getPath()))
			.error(android.R.drawable.ic_dialog_alert)
			.into(holder.pic);
		}
		return convertView;
	}
	
	private class ViewHolder {
		TextView name;
		ImageView pic;
	}
}
