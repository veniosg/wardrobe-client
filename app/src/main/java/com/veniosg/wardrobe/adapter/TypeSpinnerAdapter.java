package com.veniosg.wardrobe.adapter;

import java.util.List;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.veniosg.wardrobe.entity.Type;

public class TypeSpinnerAdapter extends ArrayAdapter<Type> {

	public TypeSpinnerAdapter(Context context, int textViewResourceId,
			List<Type> objects) {
		super(context, textViewResourceId, objects);
	}

	@Override
	public long getItemId(int position) {
		return super.getItem(position).getId();
	}
	
	@Override
	public boolean hasStableIds() {
		return true;
	}
}
