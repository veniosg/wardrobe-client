package com.veniosg.wardrobe.adapter;

import java.util.List;

import android.content.Context;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.entity.Garment;

public class CheckableGarmentGridAdapter extends GarmentGridAdapter {

	public CheckableGarmentGridAdapter(Context c, List<Garment> t) {
		super(c, t);
		mItemLayoutResource = R.layout.item_grid_garment_checkable;
	}

}
