package com.veniosg.wardrobe.net.service;

import java.lang.reflect.Type;
import java.util.Date;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public abstract class ServiceFactory {
	// REST API host location
	public static final String HOST = "http://youraddresshere";
	
	// Service singletons
	private static GarmentService garmentService;
	private static TypeService typeService;
	private static CollectionService collectionService;
	private static OutfitService outfitService;
	private static PictureService pictureService;
	
	private static RequestInterceptor requestInterceptor;
	private static RequestInterceptor pictureRequestInterceptor;
	private static GsonConverter gsonConverter;
	static {
		requestInterceptor = new RequestInterceptor() {
			public void intercept(RequestFacade req) {
				req.addHeader("Content-Type", "application/json");
				req.addHeader("Accept", "application/json");
			}
		};
		pictureRequestInterceptor = new RequestInterceptor() {
			public void intercept(RequestFacade req) {
				req.addHeader("Accept", "application/json");
			}
		};
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
			 public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				 return new Date(json.getAsJsonPrimitive().getAsLong()); 
			 }
		});
		gsonBuilder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {
			public JsonElement serialize(Date date, Type typeOfT, JsonSerializationContext context) {
				return new JsonPrimitive(date.getTime());
			}
		});
		gsonConverter = new GsonConverter(gsonBuilder.create());
	}
	
	// Enforce non-instantiability
	private ServiceFactory(){};
	
	public static GarmentService getGarmentService() {
		// Lazy loaded & cached
		if(garmentService == null) {
			garmentService = new RestAdapter.Builder()
			.setServer(HOST)
			.setRequestInterceptor(requestInterceptor)
			.setConverter(gsonConverter)
			.build()
			.create(GarmentService.class);
		}
		
		return garmentService;
	}

	public static TypeService getTypeService() {
		// Lazy loaded & cached
		if(typeService == null) {
			typeService = new RestAdapter.Builder()
			.setServer(HOST)
			.setRequestInterceptor(requestInterceptor)
			.setConverter(gsonConverter)
			.build()
			.create(TypeService.class);
		}
		
		return typeService;
	}

	public static CollectionService getCollectionService() {
		// Lazy loaded & cached
		if(collectionService == null) {
			collectionService = new RestAdapter.Builder()
			.setServer(HOST)
			.setRequestInterceptor(requestInterceptor)
			.setConverter(gsonConverter)
			.build()
			.create(CollectionService.class);
		}
		
		return collectionService;
	}

	public static OutfitService getOutfitService() {
		// Lazy loaded & cached
		if(outfitService == null) {
			outfitService = new RestAdapter.Builder()
			.setServer(HOST)
			.setRequestInterceptor(requestInterceptor)
			.setConverter(gsonConverter)
			.build()
			.create(OutfitService.class);
		}
		
		return outfitService;
	}
	
	public static PictureService getPictureService() {
		// Lazy loaded & cached
		if(pictureService == null) {
			pictureService = new RestAdapter.Builder()
			.setServer(HOST)
			.setRequestInterceptor(pictureRequestInterceptor)
			.setConverter(gsonConverter)
			.build()
			.create(PictureService.class);
		}
		
		return pictureService;
	}
}
