package com.veniosg.wardrobe.net.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedOutput;
import android.content.Context;
import android.net.Uri;

import com.veniosg.wardrobe.entity.ImageUploadResponse;
import com.veniosg.wardrobe.entity.Picture;

public interface PictureService {
	@GET("/wardrobe/picture")
	void list(Callback<List<Picture>> cb);

	@PUT("/wardrobe/picture/update")
	Response update(@Body Picture picture);

	@PUT("/wardrobe/picture/update")
	void update(@Body Picture picture, Callback<Response> cb);

	@POST("/wardrobe/picture")
	void create(@Body Picture picture, Callback<Picture> cb);

	@DELETE("wardrobe/picture/{id}")
	void delete(@Path("id") Integer id, Callback<Response> cb);
	
	@Multipart
	@POST("/wardrobe/picture/image")
	ImageUploadResponse uploadImage(@Part("image") TypedOutput image);
	
	public static class TypedImageOutput implements TypedOutput {
		private static final int BUFFER_SIZE = 4096;	// Copied from retrofit's implementation of TypedFile.
		
		private String name;
		private String mimeType;
		private Uri imageUri;
		private Context ctx;
		
		public TypedImageOutput(Context ctx, String mimeType, Uri imageUri) {
			this.ctx = ctx;
			this.mimeType = mimeType;
			this.imageUri = imageUri;
			
			List<String> pSeg = imageUri.getPathSegments();
			this.name = pSeg.get(pSeg.size()-1);
			
			System.out.println(name);
			System.out.println(imageUri.toString());
		}
		
		public String fileName() {
			return name;
		}

		// This might block the thread as it needs to open the stream.. 
		public long length() {
			InputStream in;
			int available = 0;
			
			try {
				in = ctx.getContentResolver().openInputStream(imageUri);
				available = in.available();
				in.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return available;
		}

		public String mimeType() {
			return mimeType;
		}

		public void writeTo(OutputStream out) throws IOException {
			byte[] buffer = new byte[BUFFER_SIZE];
		    
			InputStream in = ctx.getContentResolver().openInputStream(imageUri);
			try {
		      int read;
		      while ((read = in.read(buffer)) != -1) {
		        out.write(buffer, 0, read);
		      }
		    } finally {
		    	in.close();
		    }
		}
		
	}
}
