package com.veniosg.wardrobe.net.service;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

import com.veniosg.wardrobe.entity.GenerationOptions;
import com.veniosg.wardrobe.entity.Outfit;

public interface OutfitService {
	@GET("/wardrobe/outfit")
	void list(Callback<List<Outfit>> cb);

	@PUT("/wardrobe/outfit/update")
	void update(@Body Outfit outfit, Callback<Response> cb);

	@POST("/wardrobe/outfit")
	void create(@Body Outfit outfit, Callback<Outfit> cb);

	@DELETE("/wardrobe/outfit/{id}")
	void delete(@Path("id") Integer id, Callback<Response> cb);

	@GET("/wardrobe/outfit/generated")
	void generated(@Query(value = "opts") GenerationOptions opts, Callback<Outfit> cb);
}
