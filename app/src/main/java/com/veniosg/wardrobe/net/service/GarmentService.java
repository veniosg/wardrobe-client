package com.veniosg.wardrobe.net.service;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

import com.veniosg.wardrobe.entity.Garment;

public interface GarmentService {
	@GET("/wardrobe/garment")
	void list(Callback<List<Garment>> cb);

	@PUT("/wardrobe/garment/update")
	void update(@Body Garment garment, Callback<Response> cb);

	@POST("/wardrobe/garment")
	void create(@Body Garment garment, Callback<Garment> cb);

	@DELETE("wardrobe/garment/{id}")
	void delete(@Path("id") Integer id, Callback<Response> cb);
}
