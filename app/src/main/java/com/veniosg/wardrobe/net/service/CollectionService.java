package com.veniosg.wardrobe.net.service;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

import com.veniosg.wardrobe.entity.Collection;

public interface CollectionService {
	@GET("/wardrobe/collection")
	void list(Callback<List<Collection>> cb);

	@PUT("/wardrobe/collection/update")
	void update(@Body Collection collection, Callback<Response> cb);

	@POST("/wardrobe/collection")
	void create(@Body Collection collection, Callback<Collection> cb);

	@DELETE("wardrobe/collection/{id}")
	void delete(@Path("id") Integer id, Callback<Response> cb);

}
