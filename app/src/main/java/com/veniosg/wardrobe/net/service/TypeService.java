package com.veniosg.wardrobe.net.service;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

import com.veniosg.wardrobe.entity.Type;

public interface TypeService {
	@GET("/wardrobe/type")
	void list(Callback<List<Type>> cb);

	@PUT("/wardrobe/type/update")
	void update(@Body Type type, Callback<Response> cb);

	@POST("/wardrobe/type")
	void create(@Body Type type, Callback<Type> cb);

	@DELETE("wardrobe/type/{id}")
	void delete(@Path("id") Integer id, Callback<Response> cb);
}
