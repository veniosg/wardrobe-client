package com.veniosg.wardrobe.entity;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Collection implements Parcelable {
    private Long id;
    private String title;
    private List<Garment> garments;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<Garment> getGarments() {
		return garments;
	}

	public void setGarments(List<Garment> garments) {
		this.garments = garments;
	}

    protected Collection(Parcel in) {
        id = in.readLong();
        title = in.readString();
        
        garments = new ArrayList<Garment>();
        in.readList(garments, Garment.class.getClassLoader());
    }

    public Collection() {}

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeList(garments);
    }

    public static final Creator<Collection> CREATOR = new Creator<Collection>() {
        public Collection createFromParcel(Parcel in) {
            return new Collection(in);
        }

        public Collection[] newArray(int size) {
            return new Collection[size];
        }
    };
}