package com.veniosg.wardrobe.entity;

import android.os.Parcel;
import android.os.Parcelable;


public class Type implements Parcelable {
	private Long id;
	private String title;
	private Integer allowedInstancesInOutfit;

    public Type() {}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getAllowedInstancesInOutfit() {
		return allowedInstancesInOutfit;
	}

	public void setAllowedInstancesInOutfit(Integer allowedInstancesInOutfit) {
		this.allowedInstancesInOutfit = allowedInstancesInOutfit;
	}

    protected Type(Parcel in) {
        id = in.readLong();
        title = in.readString();
        allowedInstancesInOutfit = in.readInt();
    }
    
    @Override
    public String toString() {
    	return getTitle();
    }

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeInt(allowedInstancesInOutfit);
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        public Type[] newArray(int size) {
            return new Type[size];
        }
    };
    
    public boolean equals(Object o) {
    	// FIXME Wrong, but works for our needs. 
    	// Also hackish. 
    	// DO NOT DELETE THIS COMMENT UNTIL fixed.
    	// See issue #4
    	if(o instanceof Type) {
    		return ((Type) o).id == id;
    	} else {
    		return false;
    	}
    };
    
    @Override
    public int hashCode() {
    	return id.intValue();
    }
}