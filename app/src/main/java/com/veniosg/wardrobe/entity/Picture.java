package com.veniosg.wardrobe.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Picture implements Parcelable {
	private Long id;
	private String path;
	private Integer color;
	
    public Picture() {}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Integer getColor() {
		return color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

    protected Picture(Parcel in) {
        id = in.readLong();
        path = in.readString();
        color = in.readInt();
    }

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(path);
        dest.writeInt(color);
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>() {
        public Picture createFromParcel(Parcel in) {
            return new Picture(in);
        }

        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };
}