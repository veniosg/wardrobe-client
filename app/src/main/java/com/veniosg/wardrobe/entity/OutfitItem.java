package com.veniosg.wardrobe.entity;

import android.os.Parcel;
import android.os.Parcelable;


public class OutfitItem implements Parcelable {
    private Long id;
    private Float xCoord;
    private Float yCoord;
    private Float scale;
    private Float rotation;
    private Garment garment;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Float getxCoord() {
		return xCoord;
	}

	public void setxCoord(Float xCoord) {
		this.xCoord = xCoord;
	}

	public Float getyCoord() {
		return yCoord;
	}

	public void setyCoord(Float yCoord) {
		this.yCoord = yCoord;
	}

	public Float getScale() {
		return scale;
	}

	public void setScale(Float scale) {
		this.scale = scale;
	}

	public Float getRotation() {
		return rotation;
	}

	public void setRotation(Float rotation) {
		this.rotation = rotation;
	}

	public Garment getGarment() {
		return garment;
	}

	public void setGarment(Garment garment) {
		this.garment = garment;
	}

    protected OutfitItem(Parcel in) {
        id = in.readLong();
        if(id == -1) {
        	id = null;
        }
        
        xCoord = in.readFloat();
        yCoord = in.readFloat();
        scale = in.readFloat();
        rotation = in.readFloat();
        garment = in.readParcelable(Garment.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id == null ? -1 : id);
        dest.writeFloat(xCoord);
        dest.writeFloat(yCoord);
        dest.writeFloat(scale);
        dest.writeFloat(rotation);
        dest.writeParcelable(garment, 0);
    }

    public static final Creator<OutfitItem> CREATOR = new Creator<OutfitItem>() {
        public OutfitItem createFromParcel(Parcel in) {
            return new OutfitItem(in);
        }

        public OutfitItem[] newArray(int size) {
            return new OutfitItem[size];
        }
    };
}