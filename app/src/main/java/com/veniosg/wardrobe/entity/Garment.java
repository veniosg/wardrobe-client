package com.veniosg.wardrobe.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Garment implements Parcelable {
    private Long id;
    private Weather weather;
    private String name;
    private Picture picture;
    private Type type;
    private List<String> tags;
    private Boolean recentlyPurchased = Boolean.FALSE;
    
    private Date created;

    public Garment(){
    	weather = Weather.NORMAL;
    	picture = new Picture();
    	type = new Type();
    	tags = new ArrayList<String>();
    };
    
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Weather getWeather() {
		return weather;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Picture getPicture() {
		return picture;
	}

	public void setPicture(Picture picture) {
		this.picture = picture;
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public Boolean getRecentlyPurchased() {
		return recentlyPurchased;
	}

	public void setRecentlyPurchased(Boolean recentlyPurchased) {
		this.recentlyPurchased = recentlyPurchased;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public static enum Weather {
		COLD, NORMAL, HOT, ANY
	}

    protected Garment(Parcel in) {
        id = in.readLong();
        name = in.readString();
        picture = in.readParcelable(Picture.class.getClassLoader());
        tags = new ArrayList<String>();
        in.readList(tags, String.class.getClassLoader());
        type = in.readParcelable(Type.class.getClassLoader());
        weather = Weather.values()[in.readInt()];
        recentlyPurchased = in.readInt() == 1;
        created = new Date(in.readLong());
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeParcelable(picture, 0);
        dest.writeList(tags);
        dest.writeParcelable(type, 0);
        dest.writeInt(weather.ordinal());
        dest.writeInt(recentlyPurchased ? 1 : 0);
        dest.writeLong(created.getTime());
    }

    public static final Creator<Garment> CREATOR = new Creator<Garment>() {
        public Garment createFromParcel(Parcel in) {
            return new Garment(in);
        }

        public Garment[] newArray(int size) {
            return new Garment[size];
        }
    };
    
    public boolean equals(Object o) {
    	// FIXME Wrong, but works for our needs. 
    	// Also hackish. 
    	// DO NOT DELETE THIS COMMENT UNTIL fixed.
    	// See issue #4
    	if(o instanceof Garment) {
    		return ((Garment) o).id == id;
    	} else {
    		return false;
    	}
    };
    
    @Override
    public int hashCode() {
    	return id.intValue();
    }
}