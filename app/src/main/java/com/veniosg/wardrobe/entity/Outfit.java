package com.veniosg.wardrobe.entity;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Outfit implements Parcelable {
    private Long id;
    private List<OutfitItem> items;
    private String title;

    public Outfit() {
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public List<OutfitItem> getItems() {
		return items;
	}

	public void setItems(List<OutfitItem> items) {
		this.items = items;
	}

    protected Outfit(Parcel in) {
        id = in.readLong();
        if(id == -1) {
        	id = null;
        }
        
        title = in.readString();
        items = new ArrayList<OutfitItem>();
        in.readList(items, OutfitItem.class.getClassLoader());
    }

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id == null ? -1 : id);
        dest.writeString(title);
        dest.writeList(items);
    }

    public static final Creator<Outfit> CREATOR = new Creator<Outfit>() {
        public Outfit createFromParcel(Parcel in) {
            return new Outfit(in);
        }

        public Outfit[] newArray(int size) {
            return new Outfit[size];
        }
    };
}