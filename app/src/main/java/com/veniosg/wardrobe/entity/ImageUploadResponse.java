package com.veniosg.wardrobe.entity;

import android.os.Parcel;
import android.os.Parcelable;


public class ImageUploadResponse {
	private String path;
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getPath(){
		return path;
	}

    protected ImageUploadResponse(Parcel in) {
        path = in.readString();
    }

    public ImageUploadResponse() {}

	public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
    }

    public static final Parcelable.Creator<ImageUploadResponse> CREATOR = new Parcelable.Creator<ImageUploadResponse>() {
        public ImageUploadResponse createFromParcel(Parcel in) {
            return new ImageUploadResponse(in);
        }

        public ImageUploadResponse[] newArray(int size) {
            return new ImageUploadResponse[size];
        }
    };
}
