package com.veniosg.wardrobe.entity;

import com.google.gson.Gson;
import com.veniosg.wardrobe.entity.Garment.Weather;

/**
 * Parameters for the call to outfit/generated
 */
public class GenerationOptions {
	private Weather weather;
	private int maxItems;
	
	public Weather getWeather() {
		return weather;
	}
	public void setWeather(Weather weather) {
		this.weather = weather;
	}
	public int getMaxItems() {
		return maxItems;
	}
	public void setMaxItems(int maxItems) {
		this.maxItems = maxItems;
	}
	
	@Override
	public String toString() {
		Gson gc = new Gson();
		return gc.toJson(this)
				.toString();
	}
}
