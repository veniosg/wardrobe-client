package com.veniosg.wardrobe.view;

import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.veniosg.wardrobe.adapter.OutfitEditViewAdapter;
import com.veniosg.wardrobe.entity.OutfitItem;

/**
 * A widget that acts as a container of selectable child views. Used specifically for displaying outfits. <br/>
 * <br/>
 * Even though this is a View layer class, it provides a way to change the model (albeit indirectly), by means of user-touch for item order
 * and of the #setSelectedItem*() API for selected-item attribute changes. <br/>
 * Controller components containing views of this kind should use the supplied adapter to get current model state. <br/>
 * 
 * @author George Venios
 *
 */
public class OutfitDisplayView extends AdapterView<Adapter> {
	public static final float SELECTED_ALPHA_PERCENTAGE = 1.0F;
	public static final float UNSELECTED_ALPHA_PERCENTAGE = 0.65F;
	
	private OutfitEditViewAdapter mAdapter;
	private boolean mIsEditable = true;
	// Called when a child is long clicked (and the finger lifted).
	private OnLongClickListener mChildLongClickListener;
	// Called when a child is clicked and the finger lifted.
	private OnClickListener mChildClickListener;
	// Called when a child is touched and has now come to front, but the gesture has not finished.
	private OnItemSelectedListener mChildSelectedListener;
	private DataSetObserver mDataSetObserver = new DataSetObserver() {
		@Override
		public void onChanged() {
			dataChanged = true;
			
			// Force relayout
			requestLayout();
			invalidate();
		}
		
		@Override
		public void onInvalidated() {
			dataChanged = true;
			requestLayout();
			invalidate();
		}
	};
	private GestureDetectorCompat mGestureDetector = new GestureDetectorCompat(getContext(), new OnGestureListener() {
		public boolean onSingleTapUp(MotionEvent e) {
			View child = getChildAt(e.getX(), e.getY());
			if(child != null && mChildClickListener != null) {
				mChildClickListener.onClick(child);
				return true;
			}
			return false;
		}

		public void onShowPress(MotionEvent e) {
			// Unneeded because of what we do in onDown()
		}
		
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			// We're a display-view, don't listen to scrolls
			return false;
		}
		
		public void onLongPress(MotionEvent e) {
			View child = getChildAt(e.getX(), e.getY());
			if(child != null && mChildLongClickListener != null) {
				mChildLongClickListener.onLongClick(child);
			}
		}
		
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			// Don't need this
			return false;
		}
		
		public boolean onDown(MotionEvent e) {
			View item = getChildAt(e.getX(), e.getY());
			if(item != null) {
				setSelection(indexOfChild(item));
				return true;
			}
			return false;
		}
	});
	
	public boolean dataChanged = false;
	
	public OutfitDisplayView(Context context) {
		super(context);
		setFocusable(true);
		setClickable(true);
		setChildrenDrawingOrderEnabled(true);
	}

	public OutfitDisplayView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true);
		setClickable(true);
		setChildrenDrawingOrderEnabled(true);
	}

	public OutfitDisplayView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setFocusable(true);
		setClickable(true);
		setChildrenDrawingOrderEnabled(true);
	}
	
	/**
	 * Allow or disallow editing of the contents of this view by touch and/or {@link #setSelection(int)}.
	 * @param isEditable Whether to allow editing of the contents of this view. Default is false.
	 */
	public void setEditable(boolean isEditable) {
		mIsEditable = isEditable;
	}
	
	/**
	 * Set a listener to be called when a child is clicked and the finger lifted.
	 */
	public void setOnChildClickListener(OnClickListener listener) {
		mChildClickListener = listener;
	}
	
	/**
	 * Set a listener to be called when a child is long clicked (and the finger lifted).
	 */
	public void setOnChildLongClickListener(OnLongClickListener listener) {
		mChildLongClickListener = listener;
	}
	
	/**
	 * Set a listener to be called when a child is touched and has now come to the front, but the gesture has not finished.
	 */
	public void setOnChildSelectedListener(OnItemSelectedListener listener) {
		mChildSelectedListener = listener;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		
		if(mAdapter != null) {
			mAdapter.unregisterDataSetObserver(mDataSetObserver);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(mIsEditable) {
			// Release the last dragged view reference.
			if(event.getAction() == MotionEvent.ACTION_UP){
//				currentlyDraggedItem = null;
			}
			
			// Delegate to detector
			return mGestureDetector.onTouchEvent(event);
		} else {
			return super.onTouchEvent(event);
		}
	}
	
	/**
	 * Needs to be FAST. Currently it's not.
	 * @param x The x coordinate in this view.
	 * @param y The y coordinate in this view.
	 * @return The highest (in terms of z-order) view in this ViewGroup that overlaps the (x, y) point.
	 */
	private View getChildAt(float x, float y) {
		View cur;
		// Iterating from top to bottom (aka 0 -> size).
		for(int i = 0; i < getChildCount(); i++) {
			cur = getChildAt(i);
			
			if (isPointContainedIn(x, y, cur)) {
				return cur;
			}
		}
		
		// Not found.
		return null;
	}

	/**
	 * Check whether a point is is contained in the passed view.
	 * @param x A x coordinate in this view.
	 * @param y A y coordinate in this view.
	 * @param view The view to check containment on. Must be a child of this ViewGroup.
	 * @return Whether a point is is contained in the passed view.
	 */
	private boolean isPointContainedIn(float x, float y, View view) {
		Rect rect = new Rect();
		
		if(view instanceof ImageView 
				&& ((ImageView) view).getDrawable() != null) {
			// If imageview use the image's bounds
			((ImageView) view).getDrawable().copyBounds(rect);
		} else {
			// Else use the whole view's bounds
			view.getDrawingRect(rect);
		}
		RectF transRect = new RectF(rect);
		view.getMatrix().mapRect(transRect);
		return x >= transRect.left && x < transRect.right
				&& y >= transRect.top && y < transRect.bottom;
	}

	@Override
	public OutfitEditViewAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public View getSelectedView() {
		// By definition, the top view is the selected one.
		return getChildAt(0);
	}

	@Override
	public void setAdapter(Adapter adapter) {
		if(! (adapter instanceof OutfitEditViewAdapter)) {
			throw new IllegalArgumentException("Adapter MUST be an OutfitViewAdapter subclass");
		} else {
			this.mAdapter = (OutfitEditViewAdapter) adapter;
			mAdapter.registerDataSetObserver(mDataSetObserver);
			
			dataChanged = true;
			setSelection(0);	// This also redraws everything
		}
	}

	@Override
	public void setSelection(int position) {
		if(!mIsEditable) {
			return;
		}		
		
		// Bring to front in both the dataset. 
		// This will trigger a view refresh too through the DataSetObserver
		mAdapter.bringToFront(position);
		
		// Inform outside listeners
		if(mChildSelectedListener != null) {
			mChildSelectedListener.onItemSelected(OutfitDisplayView.this, getChildAt(0), 0, mAdapter.getItemId(0));		// It's at the top now. Use 0 as index.
		}
	}

	public List<OutfitItem> getItems() {
		return mAdapter.getItems();
	}
	
	@Override
	// For explanation on what we're doing here see: 
	// http://developer.sonymobile.com/2010/05/20/android-tutorial-making-your-own-3d-list-part-1/.
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		
		if(mAdapter == null) {
			return;
		}
		
		// If the dataset has changed, clear and readd children
		if(dataChanged) {
			removeAllViewsInLayout();
			
			// Add views for all outfit items
			for (int i = 0; i < mAdapter.getCount(); i++) {
				View view = mAdapter.getView(i, null, OutfitDisplayView.this);
				 
				view.setFocusable(false);
				view.setClickable(false);
				view.setLongClickable(false);
				view.setAlpha(i == 0 ? SELECTED_ALPHA_PERCENTAGE : UNSELECTED_ALPHA_PERCENTAGE);
				
				addAndMeasureChild(view);
			}
			dataChanged = false;
		}
		
		positionItems();
	}

	private void addAndMeasureChild(View view) {
		LayoutParams params = view.getLayoutParams();
	    if (params == null) {
	        params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    }
	    addViewInLayout(view, -1, params, true);
	
	    view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
	}
	
	private void positionItems() {
        for (int index = 0; index < getChildCount(); index++) {
            View child = getChildAt(index);
            OutfitItem item = mAdapter.getItem(index);
            
            // Position it
            child.layout(0, 0, getWidth(), getHeight());
            child.setX(getWidth() * item.getxCoord() - getWidth()/2);
            child.setY(getHeight() * item.getyCoord() - getHeight()/2);
            child.setScaleX(item.getScale());
            child.setScaleY(item.getScale());
            child.setRotation(item.getRotation() * 360);
        }
	}

	/**
	 * Set the top child X coordinate.
	 * @param y Must be in [0.0, 1.0]. Percentage in x axis.
	 */
	public void setSelectedViewX(float x) {
		mAdapter.getItem(0).setxCoord(x);
		requestLayout();
		invalidate();
	}
	
	/**
	 * Set the top child Y coordinate.
	 * @param y Must be in [0.0, 1.0]. Percentage in y axis.
	 */
	public void setSelectedViewY(float y) {
		mAdapter.getItem(0).setyCoord(y);
		requestLayout();
		invalidate();
	}
	
	/**
	 * Set the top child rotation.
	 * @param rotation Percentage of rotation. Values must be in [0.0, 1.0]. 1 means a full rotation of 360 degrees.
	 */
	public void setSelectedViewRotation(float rotation) {
		mAdapter.getItem(0).setRotation(rotation);
		requestLayout();
		invalidate();
	}
	
	/**
	 * Set the top child scale.
	 * @param scale Scale of the child in percentage as defined in {@link View#setScaleX(float)}.
	 */
	public void setSelectedViewScale(float scale) {
		mAdapter.getItem(0).setScale(scale);
		requestLayout();
		invalidate();
	}
	
	@Override
	protected int getChildDrawingOrder(int childCount, int i) {
		return childCount -1 -i;
	}
}
