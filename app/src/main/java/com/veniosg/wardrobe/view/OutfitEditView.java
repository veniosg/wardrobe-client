package com.veniosg.wardrobe.view;

import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.veniosg.wardrobe.adapter.OutfitEditViewAdapter;
import com.veniosg.wardrobe.entity.OutfitItem;

/**
 * A widget that acts as a container of draggable and selectable child views. Used specifically for editting and displaying outfits.
 * @author George Venios
 *
 */
public class OutfitEditView extends AdapterView<Adapter> {
	public static final float SELECTED_ALPHA_PERCENTAGE = 1.0F;
	public static final float UNSELECTED_ALPHA_PERCENTAGE = 0.25F;
	
	private OutfitEditViewAdapter mAdapter;
	private boolean mIsEditable = true;
	// Called when a child is long clicked (and the finger lifted).
	private OnLongClickListener mChildLongClickListener;
	// Called when a child is clicked and the finger lifted.
	private OnClickListener mChildClickListener;
	// Called when a child is touched and has now come to front, but the gesture has not finished.
	private OnItemSelectedListener mChildSelectedListener;
	private DataSetObserver mDataSetObserver = new DataSetObserver() {
		@Override
		public void onChanged() {
			requestLayout();
		}
		
		@Override
		public void onInvalidated() {
			removeAllViews();
		}
	};
	public View currentlyDraggedItem = null;
	private GestureDetectorCompat mGestureDetector = new GestureDetectorCompat(getContext(), new OnGestureListener() {
		public boolean onSingleTapUp(MotionEvent e) {
			View child = getChildAt(e.getX(), e.getY());
			if(child != null && mChildClickListener != null) {
				mChildClickListener.onClick(child);
				return true;
			}
			return false;
		}

		public void onShowPress(MotionEvent e) {
			// Unneeded because of what we do in onDown()
		}
		
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			// Get currently dragged item if needed
			if(currentlyDraggedItem == null) {
				currentlyDraggedItem = getChildAt(e2.getX(), e2.getY());
			}
			
			// If we were actually dragging something
			if(currentlyDraggedItem != null) {
				currentlyDraggedItem.setTranslationX(
						currentlyDraggedItem.getTranslationX()-distanceX);
				currentlyDraggedItem.setTranslationY(
						currentlyDraggedItem.getTranslationY()-distanceY);
				return true;
			}
			return false;
		}
		
		public void onLongPress(MotionEvent e) {
			View child = getChildAt(e.getX(), e.getY());
			if(child != null && mChildLongClickListener != null) {
				mChildLongClickListener.onLongClick(child);
			}
		}
		
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
			// Don't need this
			return false;
		}
		
		public boolean onDown(MotionEvent e) {
			View item = getChildAt(e.getX(), e.getY());
			if(item != null) {
				int index = indexOfChild(item);
				setSelection(index);
				return true;
			}
			return false;
		}
	});
	
	public OutfitEditView(Context context) {
		super(context);
		setFocusable(true);
		setClickable(true);
	}

	public OutfitEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setFocusable(true);
		setClickable(true);
	}

	public OutfitEditView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setFocusable(true);
		setClickable(true);
	}
	
	/**
	 * Allow or disallow editing of the contents of this view by touch and/or {@link #setSelection(int)}.
	 * @param isEditable Whether to allow editing of the contents of this view. Default is false.
	 */
	public void setEditable(boolean isEditable) {
		mIsEditable = isEditable;
	}
	
	/**
	 * Set a listener to be called when a child is clicked and the finger lifted.
	 */
	public void setOnChildClickListener(OnClickListener listener) {
		mChildClickListener = listener;
	}
	
	/**
	 * Set a listener to be called when a child is long clicked (and the finger lifted).
	 */
	public void setOnChildLongClickListener(OnLongClickListener listener) {
		mChildLongClickListener = listener;
	}
	
	/**
	 * Set a listener to be called when a child is touched and has now come to the front, but the gesture has not finished.
	 */
	public void setOnChildSelectedListener(OnItemSelectedListener listener) {
		mChildSelectedListener = listener;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		
		if(mAdapter != null) {
			mAdapter.unregisterDataSetObserver(mDataSetObserver);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(mIsEditable) {
			// Release the last dragged view reference.
			if(event.getAction() == MotionEvent.ACTION_UP){
				currentlyDraggedItem = null;
			}
			
			// Delegate to detector
			return mGestureDetector.onTouchEvent(event);
		} else {
			return super.onTouchEvent(event);
		}
	}
	
	/**
	 * Needs to be FAST. Currently it's not.
	 * @param x The x coordinate in this view.
	 * @param y The y coordinate in this view.
	 * @return The highest (in terms of z-order) view in this ViewGroup that overlaps the (x, y) point.
	 */
	private View getChildAt(float x, float y) {
		View cur;
		// Iterating from top to bottom (aka 0 -> size).
		for(int i = 0; i < getChildCount(); i++) {
			cur = getChildAt(i);
			
			if (isPointContainedIn(x, y, cur)) {
				return cur;
			}
		}
		
		// Not found.
		return null;
	}

	/**
	 * Check whether a point is is contained in the passed view.
	 * @param x A x coordinate in this view.
	 * @param y A y coordinate in this view.
	 * @param view The view to check containment on. Must be a child of this ViewGroup.
	 * @return Whether a point is is contained in the passed view.
	 */
	private boolean isPointContainedIn(float x, float y, View view) {
		Rect rect = new Rect();
		
		if(view instanceof ImageView 
				&& ((ImageView) view).getDrawable() != null) {
			// If imageview use the image's bounds
			((ImageView) view).getDrawable().copyBounds(rect);
		} else {
			// Else use the whole view's bounds
			view.getDrawingRect(rect);
		}
		RectF transRect = new RectF(rect);
		view.getMatrix().mapRect(transRect);
		return x >= transRect.left && x < transRect.right
				&& y >= transRect.top && y < transRect.bottom;
	}

	@Override
	public OutfitEditViewAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public View getSelectedView() {
		// By definition, the top view is the selected one.
		return getChildAt(0);
	}

	@Override
	public void setAdapter(Adapter adapter) {
		if(! (adapter instanceof OutfitEditViewAdapter)) {
			throw new IllegalArgumentException("Adapter MUST be an OutfitViewAdapter subclass");
		} else {
			this.mAdapter = (OutfitEditViewAdapter) adapter;
			adapter.registerDataSetObserver(mDataSetObserver);
			
			requestLayout();
		}
	}

	@Override
	public void setSelection(int position) {
		if(!mIsEditable) {
			return;
		}
		
		// Indicate new selection
		for(int i = 0; i < getChildCount(); i++) {
			getChildAt(position).setAlpha(
					i == position 
							? SELECTED_ALPHA_PERCENTAGE
							: UNSELECTED_ALPHA_PERCENTAGE);
		}		
		
		mAdapter.bringToFront(position);			// Bring to front in the dataset.
		bringChildToFront(getChildAt(position));	// Bring to front in the view.

//		requestLayout();
		invalidate();
		
		// Inform outside listeners
		if(mChildSelectedListener != null) {
			mChildSelectedListener.onItemSelected(OutfitEditView.this, getChildAt(0), 0, mAdapter.getItemId(0));	// It's at the top now. Use 0 as index.
		}
	}

	public List<OutfitItem> getItems() {
		return mAdapter.getItems();
	}
	
	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		removeAllViewsInLayout();
		
		if(mAdapter != null) {
			// Add in reverse order so that the last is on top.
			for(int i = mAdapter.getCount()-1; i >= 0; i--) {
				View view = mAdapter.getView(i, null, this);

				view.setFocusable(false);
				view.setClickable(false);
				view.setLongClickable(false);
				addViewInLayout(view, (mAdapter.getCount()- 1) - i , view.getLayoutParams());
				view.layout(left, top, right, bottom);
			}
		}
	}
}
