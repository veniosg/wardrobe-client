package com.veniosg.wardrobe.view;

import java.util.List;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A widget that automatically adds and removes {@link EditText}s as the user types in any of them.
 * @author George Venios
 */
public class DynamicSizeEditTextGroup extends LinearLayout {
	private List<String> mItems;
	
	public DynamicSizeEditTextGroup(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public DynamicSizeEditTextGroup(Context context) {
		super(context);
	}

	public DynamicSizeEditTextGroup(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Set the contents of this view's children.
	 * @param content Content of each EditText to be created.
	 */
	public void setContentData(List<String> content) {
		mItems = content;
		
		initViewRepresentation();
	}
	
	public List<String> getContentData() {
		// Recreate mItems to pass the latest state.
		mItems.clear();
		
		for(int i = 0; i < getChildCount()-1; i++) {	// We don't care about the last one as we know it's always empty.
			CharSequence cs = ((TextView) getChildAt(i))
					.getText();
			
			if(!TextUtils.isEmpty(cs)) {
				mItems.add(cs.toString());
			}
		}
		
		return mItems;
	}
	
	private void initViewRepresentation() {
		removeAllViews();

		// Add internal views
		for(String s : mItems) {
			addView(createInternalView(s));
		}
		
		// Add last view that acts as a trigger for the addition of more fields.
		addView(createLastView());
	}
	
	private EditText createLastView() {
		EditText v = new EditText(getContext());
		v.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				// Last view was selected
				if(hasFocus) {
					final View newView = createInternalView(null);
					// Put it in place of the last field.
					addView(newView, getChildCount()-1);
					newView.requestFocus();
				}
			}
		});
		v.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NULL);
		v.setCursorVisible(false);
		
		return v;
	}
	
	private EditText createInternalView(CharSequence text) {
		EditText v = new EditText(getContext());
		v.setOnFocusChangeListener(new OnFocusChangeListener() {
			public void onFocusChange(final View v, boolean hasFocus) {
				// This view is not selected anymore and is empty
				if(!hasFocus && TextUtils.isEmpty(((EditText) v).getText())) {
					// Clear focus to stop double focus 
					// requests due to IME "next"
					clearFocus();

					// The view isn't needed, remove it.
					removeView(v);
				}
			}
		});
		v.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
		v.setFreezesText(true);
		v.setText(text);
		
		return v;
	}
}
