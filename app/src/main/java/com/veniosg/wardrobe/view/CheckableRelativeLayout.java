package com.veniosg.wardrobe.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.RelativeLayout;

public class CheckableRelativeLayout extends RelativeLayout implements Checkable {
	private boolean mChecked = false;

	public CheckableRelativeLayout(Context context) {
		super(context);
	}
	public CheckableRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	public CheckableRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public boolean isChecked() {
		return mChecked;
	}

	public void setChecked(boolean check) {
		if(mChecked != check) {
			mChecked = check;
			// Allow drawable state change to indicate selection
			setActivated(check);
		}
	}

	public void toggle() {
		setChecked(!mChecked);
	}

}
