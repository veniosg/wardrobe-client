package com.veniosg.wardrobe.activity;

import android.os.Bundle;

import com.veniosg.wardrobe.fragment.OutfitGenerateFragment;

public class OutfitGenerateActivity extends BaseActivity {
	private OutfitGenerateFragment mFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Setup fragment
		mFragment = (OutfitGenerateFragment) getSupportFragmentManager()
				.findFragmentByTag(FRAGMENT_CONTENT_TAG);
		if(mFragment == null) {
			mFragment = new OutfitGenerateFragment();
			
			// Add the fragment
			getSupportFragmentManager()
			.beginTransaction()
			.add(android.R.id.content, mFragment, FRAGMENT_CONTENT_TAG)
			.commit();
		}
	}
}
