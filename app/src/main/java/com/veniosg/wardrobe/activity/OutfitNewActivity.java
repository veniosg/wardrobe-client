package com.veniosg.wardrobe.activity;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.entity.Outfit;
import com.veniosg.wardrobe.fragment.OutfitEditFragment;

public class OutfitNewActivity extends EntityNewActivity<Outfit, OutfitEditFragment> {
	@Override
	protected OutfitEditFragment newFragment() {
		return new OutfitEditFragment();
	}
	
	@Override
	Outfit getEntityForFragment() {
		// Handle generated but not-saved use case
		Outfit outfit = getIntent().getParcelableExtra(Constants.EXTRA_ENTITY);
		if(outfit != null) {
			return outfit;
		} else {
			return super.getEntityForFragment();
		}
	}
}
