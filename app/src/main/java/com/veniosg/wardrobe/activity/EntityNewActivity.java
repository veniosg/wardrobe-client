package com.veniosg.wardrobe.activity;

import android.os.Bundle;
import android.os.Parcelable;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.fragment.EntityEditFragment;

/**
 * Use this class by subclassing and implementing {@link #newFragment()}. To create "Editor" Activities subclass {@link EntityEditActivity}. <br/> <br/>
 * George's rant: Silly Java limitations. I was THAT close to only needing this class for EVERY ENTITY'S CREATION AND EDITING. Anyway..
 * @author George Venios
 *
 * @param <ET> Type of the entity this activity should handle.
 * @param <FT> Type of the {@link EntityEditFragment<T>} subclass.
 */
public abstract class EntityNewActivity<ET extends Parcelable, FT extends EntityEditFragment<ET>> extends BaseActivity {
	private FT mFragment;

	@SuppressWarnings("unchecked")
	@Override
	protected final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Setup fragment
		mFragment = (FT) getSupportFragmentManager().findFragmentByTag(FRAGMENT_CONTENT_TAG);
		if(mFragment == null) {
			mFragment = newFragment();									// NOTE: Java can't do "new T()" if T is a generic type and neither can it have static abstract 
																		// methods in abstract classes and we can't enforce a static factory method on FT's subclasses.
			Bundle args = new Bundle();
			args.putParcelable(Constants.ARG_ENTITY, getEntityForFragment());
			mFragment.setArguments(args);
			
			// Add
			getSupportFragmentManager()
			.beginTransaction()
			.add(android.R.id.content, mFragment, FRAGMENT_CONTENT_TAG)
			.commit();
		}
	}

	/**
	 * Use this method to provide the correct ET instance to the underlying {@link EntityEditFragment<T>}. <br/>
	 * By default this returns null.
	 * @return An ET instance or null to indicate that the fragment must create a new ET entity.
	 */
	ET getEntityForFragment() {
		return null;
	}
	
	/**
	 * Override this to provide the Fragment instance 
	 * that will be displayed as part of this Activity's content. <br/>
	 * This will be passed the entity internally, as defined in {@link #getEntityForFragment()}.
	 * @return 
	 */
	protected abstract FT newFragment();
}
