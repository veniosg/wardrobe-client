package com.veniosg.wardrobe.activity;

import com.veniosg.wardrobe.entity.Collection;
import com.veniosg.wardrobe.fragment.CollectionEditFragment;

public class CollectionNewActivity extends EntityNewActivity<Collection, CollectionEditFragment> {
	@Override
	protected CollectionEditFragment newFragment() {
		return new CollectionEditFragment();
	}
}
