package com.veniosg.wardrobe.activity;

import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.fragment.GarmentEditFragment;


public class GarmentEditActivity extends EntityEditActivity<Garment, GarmentEditFragment> {

	@Override
	protected GarmentEditFragment newFragment() {
		return new GarmentEditFragment();
	}
}
