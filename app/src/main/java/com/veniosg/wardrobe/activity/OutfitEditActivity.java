package com.veniosg.wardrobe.activity;

import com.veniosg.wardrobe.entity.Outfit;
import com.veniosg.wardrobe.fragment.OutfitEditFragment;


public class OutfitEditActivity extends EntityEditActivity<Outfit, OutfitEditFragment> {
	@Override
	protected OutfitEditFragment newFragment() {
		return new OutfitEditFragment();
	}
}
