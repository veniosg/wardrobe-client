package com.veniosg.wardrobe.activity;

import android.os.Parcelable;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.fragment.EntityEditFragment;


/**
 * This Activity acts as a wiring base for calling the fragment that will actually do the editing. Subclasses only need to implement {@link #newFragment()}.
 * @author George Venios
 * 
 * @see EntityNewActivity
 */
public abstract class EntityEditActivity<ET extends Parcelable, FT extends EntityEditFragment<ET>> 
				extends EntityNewActivity<ET, FT> {
	@Override
	ET getEntityForFragment() {
		if(!getIntent().hasExtra(Constants.EXTRA_ENTITY)){
			throw new IllegalArgumentException("Can't call EntityEditActivity without an entity. Use Constants.EXTRA_ENTITY.");
		}
		
		return getIntent().getExtras().getParcelable(Constants.EXTRA_ENTITY);
	}
}
