package com.veniosg.wardrobe.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.veniosg.wardrobe.R;
import com.veniosg.wardrobe.fragment.CollectionGridFragment;
import com.veniosg.wardrobe.fragment.GarmentGridFragment;
import com.veniosg.wardrobe.fragment.OutfitGridFragment;
import com.veniosg.wardrobe.fragment.TypeGridFragment;

public class HomeActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// Start off at Garments' page
		mViewPager.setCurrentItem(1);
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		String[] mTitles;

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
			
			mTitles = getResources().getStringArray(R.array.titles_home);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				return new OutfitGridFragment();
			case 1:
				return new GarmentGridFragment();
			case 2:
				return new CollectionGridFragment();
			case 3:
				return new TypeGridFragment();
			default:
				return null;	// Let it crash
			}

		}

		@Override
		public int getCount() {
			return mTitles.length;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mTitles[position].toUpperCase();
		}
	}
}
