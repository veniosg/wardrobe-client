package com.veniosg.wardrobe.activity;

import android.os.Bundle;

import com.veniosg.wardrobe.Constants;
import com.veniosg.wardrobe.entity.Garment;
import com.veniosg.wardrobe.fragment.GarmentDebugFragment;


public class GarmentDebugActivity extends BaseActivity {
	private GarmentDebugFragment mFragment;

	@Override
	protected final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Setup fragment
		mFragment = (GarmentDebugFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_CONTENT_TAG);
		if(mFragment == null) {
			mFragment = new GarmentDebugFragment();
			Bundle args = new Bundle();
			args.putParcelable(Constants.ARG_ENTITY, getEntityForFragment());
			mFragment.setArguments(args);
			
			// Add
			getSupportFragmentManager()
			.beginTransaction()
			.add(android.R.id.content, mFragment, FRAGMENT_CONTENT_TAG)
			.commit();
		}
	}

	/**
	 * Use this method to provide the correct ET instance to the underlying {@link EntityEditFragment<T>}. <br/>
	 * By default this returns null.
	 * @return An ET instance or null to indicate that the fragment must create a new ET entity.
	 */
	Garment getEntityForFragment() {
		return getIntent().getParcelableExtra(Constants.EXTRA_ENTITY);
	}
}