package com.veniosg.wardrobe.activity;

import com.veniosg.wardrobe.entity.Type;
import com.veniosg.wardrobe.fragment.TypeEditFragment;

public class TypeNewActivity extends EntityNewActivity<Type, TypeEditFragment> {
	
	@Override
	protected TypeEditFragment newFragment() {
		return new TypeEditFragment();
	}
}
