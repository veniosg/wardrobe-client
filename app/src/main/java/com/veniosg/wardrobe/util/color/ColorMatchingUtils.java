package com.veniosg.wardrobe.util.color;

import android.graphics.Color;

/**
 * See http://www.worqx.com/color/combinations.htm <br/>
 * Also see http://dandyfashioner.blogspot.gr/2009/09/art-of-color-what-colors-go-well.html
 * @author George Venios
 */
public abstract class ColorMatchingUtils {
	/**
	 * The default tolerance to use for grayscale check. 
	 * If a color has saturation of value STANDARD_GRAYSCALE_TOLERANCE
	 * or less, this means it can be considered grayscale. 
	 */
	private static final float STANDARD_GRAYSCALE_TOLERANCE = 0.17F;
	/**
	 * In how many bands we will divide the RYB spectrum. 
	 */
	public static final int STANDARD_NUM_BANDS = 12;
	
	private ColorMatchingUtils() {}
	
	public static boolean doGarmentColorsMatch(int color1, int color2) {
		// TODO account for jeans and khaki pants
		// TODO account for brown shoes..?
		
		if (isGrayscale(color1) || isGrayscale(color2)) {
			return true;
		} else if (areComplementary(color1, color2)) {
			return true;
		} else if (areTriadicElements(color1, color2)) {
			return true;
		} else if (areSplitComplementary(color1, color2)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean areComplementary(int color1, int color2) {
		return ColorWheel.getComplementaryWithBands(color1, STANDARD_NUM_BANDS) 
				== ColorWheel.getBandOfColor(color2, STANDARD_NUM_BANDS);
	}
	
	public static boolean areTriadicElements(int color1, int color2) {
		int color2Band = ColorWheel.getBandOfColor(color2, STANDARD_NUM_BANDS);
		
		return ColorWheel.getColorAtWithBands(color1, 120, STANDARD_NUM_BANDS) == color2Band
				|| ColorWheel.getColorAtWithBands(color1, 240, STANDARD_NUM_BANDS) == color2Band;
	}
	
	public static boolean areTriadic(int color1, int color2, int color3) {
		int color2Band = ColorWheel.getBandOfColor(color2, STANDARD_NUM_BANDS);
		int color3Band = ColorWheel.getBandOfColor(color3, STANDARD_NUM_BANDS);
		int colorAt120Band = ColorWheel.getColorAtWithBands(color1, 120, STANDARD_NUM_BANDS);
		int colorAt240Band = ColorWheel.getColorAtWithBands(color1, 240, STANDARD_NUM_BANDS);

		return (color2Band == colorAt120Band && color3Band == colorAt240Band)
				|| (color3Band == colorAt120Band && color2Band == colorAt120Band);
	}
	
	public static boolean areSplitComplementary(int color1, int color2) {
		int color2Band = ColorWheel.getBandOfColor(color2, STANDARD_NUM_BANDS);
		
		return ColorWheel.getColorAtWithBands(color1, 180+30, STANDARD_NUM_BANDS) == color2Band 
				|| ColorWheel.getColorAtWithBands(color1, 180-30, STANDARD_NUM_BANDS) == color2Band;
	}
	
	public static boolean isGrayscale(int color) {
		return isCloseToGrayscale(STANDARD_GRAYSCALE_TOLERANCE, color);
	}
	
	private static boolean isCloseToGrayscale(float tolerancePercent, int color) {
		float[] hsbVals = new float[3];
		Color.colorToHSV(color, hsbVals);
		
		return hsbVals[1] < STANDARD_GRAYSCALE_TOLERANCE;
	}
}
