package com.veniosg.wardrobe.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import com.veniosg.wardrobe.net.service.ServiceFactory;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public abstract class Utils {
	// Enforce non-instantiability
	// Effective Java 2nd Ed., Item 4
	private Utils(){};
	
	public static boolean hasNetwork(Context c) {
		ConnectivityManager cm = (ConnectivityManager) 
				c.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo ni = cm.getActiveNetworkInfo();
		return ni != null && ni.isConnected();
	}
	
	public static boolean isIntentAvailable(Context context, String action) {
	    final PackageManager packageManager = context.getPackageManager();
	    final Intent intent = new Intent(action);
	    List<ResolveInfo> list =
	            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
	    return list.size() > 0;
	}

	/**
	 * Get the main color of the picture. Does not take into account the center of the bmp.
	 * @param bmp The picture to get the color of.
	 * @param margin The percentage of the bmp width (at the borders) that will not be taken into account.
	 * @param core The percentage of the bmp area's center that will not be taken into account.
	 * @return The main color of the picture's item.
	 */
	public static int getColorFromBitmap(Bitmap bmp, float margin, float core) {
		int currentColor = 0;

		bmp = Bitmap.createScaledBitmap(bmp, 100, 150, false);
		
		MeanValueCalculator red = new MeanValueCalculator();
		MeanValueCalculator green = new MeanValueCalculator();
		MeanValueCalculator blue = new MeanValueCalculator();

		int[] dimensions = new int[2];
		dimensions[0] = bmp.getWidth();
		dimensions[1] = bmp.getHeight();

		float[] center = new float[2];
		center[0] = dimensions[0] / 2;
		center[1] = dimensions[1] / 2;

		float[] margins = new float[2];
		margins[0] = dimensions[0] * margin;
		margins[1] = dimensions[1] * margin;

		float[] cores = new float[2];
		cores[0] = dimensions[0] * core;
		cores[1] = dimensions[1] * core;

		// loop through all rows within margins
		for (int y = Math.round(margins[1]); y < Math.round(dimensions[1] - margins[1]); y++)
			// loop through pixels of each row, excluding ones in the core.
			for (int x = Math.round(margins[0]); 
					x < Math.round(dimensions[0] - margins[0]); 
					x = Math.round(((y > center[1] - cores[1] / 2 && y < center[1] + cores[1] / 2) ? 
							((x > center[0] - cores[0] / 2 && x < center[0] + cores[0] / 2) ? center[0] + center[0] / 2 : x + 1)
							: x + 1))) {
				currentColor = bmp.getPixel(x, y);

				red.addValue(Color.red(currentColor));
				green.addValue(Color.green(currentColor));
				blue.addValue(Color.blue(currentColor));
			}

		return Color.rgb(Math.round(red.getMean()),
				Math.round(green.getMean()), Math.round(blue.getMean()));
	}
	
	public static float degreesToPercentage(int degrees) {
		return (float) degrees / 360F;
	}
	
	public static void writeBitmapToFile(Bitmap bmp, File destFile) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(bmp.getByteCount());
		try {
			destFile.mkdirs();
			destFile.createNewFile();
			baos.writeTo(new FileOutputStream(destFile));
		} catch (FileNotFoundException e) {
			// Ignore
			e.printStackTrace();
		} catch (IOException e) {
			// Ignore
			e.printStackTrace();
		}
	}
	
	/**
	 * Based on https://bitbucket.org/veniosg/wardrobe-server/issue/13/.
	 * @return The image URL to use.
	 */
	public static String getFullUrl(String part) {
		if (!part.startsWith("http://")) {
			return ServiceFactory.HOST + "/wardrobe" + part;
		} else {
			return part;
		}
	}
}