package com.veniosg.wardrobe.util.color;

import android.graphics.Color;

import com.veniosg.wardrobe.util.Utils;

/**
 * @author George Venios
 *
 */
public abstract class ColorWheel {
	private ColorWheel(){}
	
	/**
	 * Get the exact complementary color of rgbIn.
	 * @param rgbIn The start color.
	 * @return The complementary color.
	 */
	public static int getComplementary(int rgbIn) {
		return getColorAt(rgbIn, 180);
	}
	
	public static int getComplementaryWithBands(int rgbIn, int numBands) {
		return getColorAtWithBands(rgbIn, 180, numBands);
	}
	
	/**
	 * Get the color at +atDegrees of rgbIn, in the HSL color wheel.
	 * @return The int RGB color representation.
	 */
	public static int getColorAt(int rgbIn, int atDegrees) {
		// Get color in HSB
		float[] hsbAr = new float[3];
		Color.colorToHSV(rgbIn,  
				hsbAr);
		
		// Rotate 
		float h = hsbAr[0];
		h += Utils.degreesToPercentage(atDegrees);
		
		// Normalize
		h = normalizePercentage(h);
		
		// HSB to RGB
		hsbAr[0] = h;
		return Color.HSVToColor(hsbAr);
	}
	
	/**
	 * Same as getColorAt, but bands(groups) the colors in numBands colors. <br/>
	 * Values below 4 can wield unexpected results. It's a color wheel you dummy!
	 * @return The int RGB color representation.
	 */
	public static int getColorAtWithBands(int rgbIn, int atDegrees, int numBands) {
		float percentagePerBand = 1F / (float) numBands;
		
		// Get color in HSB
		float[] hsbAr = new float[3];
		Color.colorToHSV(rgbIn, hsbAr);
		
		float h = hsbAr[0];
		// Translate to band that includes this color (middle of the band)
		h = (int) (h / percentagePerBand) * percentagePerBand;	// Integer division to find the band that includes this color. 
																// Times percentage per band, to find the actual base color percentage.
		h += Utils.degreesToPercentage(atDegrees);
		h = normalizePercentage(h);
		
		// HSB to RGB
		hsbAr[0] = h;
		return Color.HSVToColor(hsbAr);
	}
	
	/**
	 * Based on the rotation of the input color and number of bands passed, 
	 * return the color of the band in a wheel with numBands distinct colors.
	 * @return The int RGB color representation.
	 */
	public static int getBandOfColor(int rgbIn, int numBands) {
		float huePerBand = 360F / (float) numBands;
		
		// Get color in HSB
		float[] hsbAr = new float[3];
		Color.colorToHSV(rgbIn, hsbAr);
		
		float h = hsbAr[0];
		// Translate to band that includes this color (middle of the band)
		h = ((int) (h / huePerBand)) * huePerBand;				// Integer division to find the band that includes this color. 
																// Times percentage per band, to find the actual base color percentage.
		h = normalizePercentage(h);
		
		// HSB to RGB
		hsbAr[0] = h;
		hsbAr[1] = 0.5F;
		hsbAr[2] = 0.5F;
		return Color.HSVToColor(hsbAr);
	}
	
	private static final float normalizePercentage(float in) {
		if(in >= 1) {
			in -= 1;
		} else if (in < 0) {
			in += 1;
		}
		
		return in;
	}
}
