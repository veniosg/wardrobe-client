package com.veniosg.wardrobe.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A list of objects that holds them sorted in a reverse time-of-selection fashion starting from index 0. <br/>
 * <br/>
 * E.g.: This list [C, B, A, E] means that the the items were selected in this order: E, A, B, C. <br/>
 * This can be helpful when drawing stacked images, as the last selected image will be on the top. <br/>
 * <br/>
 * Just use {@link #bringToFront(int)} to mark an item as currently selected. Its implementation will handle the rest.<br/>
 * @author George Venios
 */
public class ZOrderAwareList<T> extends LinkedList<T> {
	// Required by the Serializable interface of PriorityQueue<?>
	static final long serialVersionUID = 1;
	
	public ZOrderAwareList(List<T> items) {
		super(items != null ? items : new ArrayList<T>());
	}

	public ZOrderAwareList() {
		super();
	}

	/**
	 * Mark the item at index as the current selection. This will make this item come to the front of the queue. <br/>
	 * Runs in O(n) as it uses the {@link LinkedList#get(int)} method.
	 * @param index The index of the item to move.
	 */
	public final void bringToFront(int index) {
		// Keep item
		T item = get(index);
		
		// Remove from previous position
		remove(index);
		
		// Insert at the top
		addFirst(item);
	}
}
