package com.veniosg.wardrobe.util;

/**
 * Simple and efficient mean value calculator.
 * @author George Venios
 * 
 */
public class MeanValueCalculator {
	private int elements = 0;
	private float sum = 0F;

	public void addValue(float value) {
		elements++;
		sum += value;
	}

	/**
	 * Calculate the current mean value.
	 * 
	 * @return Zero if there were no values added, the mean value otherwise.
	 */
	public float getMean() {
		return elements != 0 ? 
				sum / elements : 0;
	}
}