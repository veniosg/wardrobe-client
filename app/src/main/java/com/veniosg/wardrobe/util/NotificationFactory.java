package com.veniosg.wardrobe.util;

import java.util.Date;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.veniosg.wardrobe.R;

public abstract class NotificationFactory {
	private static final int UPLOAD_NOTIFICATION_ID = 1;
	
	private NotificationFactory() {}
	
	public static final void showUploadUpdate(Context c) {
		// Show progress notification
		Notification not = new NotificationCompat.Builder(c)
				.setOngoing(true)
				.setTicker(c.getString(R.string.uploading_image))
				.setContentTitle(c.getString(R.string.uploading_image))
				.setPriority(NotificationCompat.PRIORITY_HIGH)
				.setProgress(1, 0, true)
				.setWhen(0)
				.setSmallIcon(android.R.drawable.stat_notify_sync)
				.build();
		
		NotificationManager nMan = (NotificationManager) c
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nMan.notify(UPLOAD_NOTIFICATION_ID, not);
	}
	
	public static final void showUploadFinished(final Context c) {
		// Show upload finished dialog
		Notification not = new NotificationCompat.Builder(c)
		.setContentTitle(c.getString(R.string.uploaded_image))
		.setPriority(NotificationCompat.PRIORITY_DEFAULT)
		.setWhen(new Date().getTime())
		.setSmallIcon(android.R.drawable.stat_notify_sync_noanim)
		.build();

		NotificationManager nMan = (NotificationManager) c
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nMan.notify(UPLOAD_NOTIFICATION_ID, not);
	}
}