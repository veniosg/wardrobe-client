package com.veniosg.wardrobe;

public abstract class Constants {
	private Constants(){};
	
	// ACTIVITY EXTRAS
	public static final String EXTRA_ENTITY = "com.veniosg.wardrobe.extra.ENTITY";
	
	// FRAGMENT ARGUMENTS 
	public static final String ARG_ENTITY = "com.veniosg.wardrobe.ENTITY";
	
	// MISC
	public static final String PICTURE_ALBUM_NAME = "GarmentPictures";

	public static final String ACTION_RELOAD_DATA = "com.veniosg.wardrobe.action.RELOAD_DATA";

	// MimeTypes
	public static final String MIME_TYPE = "vnd.veniosg.wardrobe/vnd.wardrobe.type";
	public static final String MIME_COLLECTION = "vnd.veniosg.wardrobe/vnd.wardrobe.collection";
	public static final String MIME_GARMENT = "vnd.veniosg.wardrobe/vnd.wardrobe.garment";
	public static final String MIME_OUTFIT = "vnd.veniosg.wardrobe/vnd.wardrobe.outfit";
	public static final String MIME_IMAGE = "image/*";
}
